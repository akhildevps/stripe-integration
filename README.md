# Stripe integration
stripe sample integration in Nodejs
## Clone project
```git clone https://gitlab.com/akhildevps/stripe-integration.git```

## Install packages
```npm install```

## Start Node Application
```node app.js```

## Open in browser
```http://localhost:3000```


### Useful site for integration
https://www.geeksforgeeks.org/how-to-integrate-stripe-payment-gateway-in-node-js/

https://stripe.com/docs/api/  
